package com.example.fatema.location.Kotlin

import com.google.android.gms.maps.model.LatLng
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class DataParser {
    internal var estimated_time = ""

    /** Receives a JSONObject and returns a list of lists containing latitude and longitude  */
    fun parse(jObject: JSONObject): List<HashMap<String, String>> {

        val routes = ArrayList<HashMap<String, String>>()
        val jRoutes: JSONArray
        var jLegs: JSONArray
        var jPolyLine: String
        //JSONArray jSteps;
        val jDuration: JSONArray

        try {

            jRoutes = jObject.getJSONArray("routes")

            /** Traversing all routes  */
            for (i in 0 until jRoutes.length()) {

                jLegs = (jRoutes.get(i) as JSONObject).getJSONArray("legs")
                jPolyLine = ((jRoutes.get(i) as JSONObject).get("overview_polyline") as JSONObject).get("points") as String
                val list = decodePoly(jPolyLine)
                for (l in list.indices) {
                    val hm = HashMap<String, String>()
                    hm["lat"] = java.lang.Double.toString(list[l].latitude)
                    hm["lng"] = java.lang.Double.toString(list[l].longitude)
                    routes.add(hm)
                }
                estimated_time = ((jLegs.get(0) as JSONObject).get("duration") as JSONObject).get("text") as String
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: Exception) {
        }


        return routes
    }


    fun getEstimatedTime(): String {
        return estimated_time
    }


    /**
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    private fun decodePoly(encoded: String): List<LatLng> {

        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }

        return poly
    }
}