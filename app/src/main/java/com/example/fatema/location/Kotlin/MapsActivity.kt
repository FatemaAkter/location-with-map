package com.example.fatema.location.Kotlin

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Geocoder
import android.location.Location
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import com.example.fatema.location.BaseAppController
import com.example.fatema.location.Listener.LocationChangedListener
import com.example.fatema.location.R
import kotlinx.android.synthetic.main.activity_maps.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.net.URL
import java.util.*

class MapsActivity : FragmentActivity(), OnMapReadyCallback, LocationChangedListener, GoogleMap.OnCameraIdleListener {

    private var mMap: GoogleMap? = null
    private var myMarker: Marker? = null
    var bearing = 0f
    var prevLocation: Location? = null
    var value: Int = 0
    private var TAG = "MapsActivity"
    var sourceLat: Double? = null
    var sourceLong: Double? = null
    var destinationLat: Double? = null
    var destinationLong: Double? = null
    var context: Context = this
    var flag = 1
    lateinit var parser: DataParser
    private var sourceMarker: Marker? = null
    private var marker: Marker? = null
    private var destinationMarker: Marker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        initMap()
        BaseAppController.getInstance().initLocationListener(this)
        BaseAppController.getInstance().initLocation()
        BaseAppController.getInstance().getLastLocation()
        BaseAppController.getInstance().startLocationUpdates()
        imageView_marker.setImageResource(R.drawable.ic_map_source_marker)

        textView_source.setOnClickListener {
            imageView_marker.setImageResource(R.drawable.ic_map_source_marker)
            flag = 1
        }

        textView_destination.setOnClickListener {
            imageView_marker.setImageResource(R.drawable.ic_map_destination_marker)
            flag = 2
        }

        button_get_direction.setOnClickListener {
            fetchUrl()
        }

    }


    fun initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        if (checkPermissions()) {
            mMap = googleMap
            val latLng = LatLng(23.810332, 100.4125181)
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(16f).build()
            mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            mMap?.setOnCameraIdleListener(this)
            mMap?.uiSettings?.isScrollGesturesEnabled = true
            mMap?.setMinZoomPreference(11f)
            mMap?.uiSettings?.isCompassEnabled = false
            mMap?.isBuildingsEnabled = true
            mMap?.isMyLocationEnabled = false
            mMap?.uiSettings?.isRotateGesturesEnabled = false
            mMap?.uiSettings?.isTiltGesturesEnabled = false

        }
        mMap?.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.style_json))


    }

    override fun onLocationChanged(location: Location) {
        Log.d("Location", "" + location)
        if (value == 0) {
            val myLocation = LatLng(location.latitude, location.longitude)
            val cameraPosition = CameraPosition.Builder().target(myLocation).zoom(16f).build()
            mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

            /*    //frmSourceLoad.setVisibility(View.VISIBLE);
              Utilities.getAddressUsingLatLng("source", frmSource, context, "" + latitude, "" + longitude, addressListener, true);*/
            value++

        }
        if (prevLocation == null) {
            prevLocation = location

        }
        bearing = prevLocation!!.bearingTo(location)

        myMarker?.remove()

        val markerOptions1 = MarkerOptions()
                .position(LatLng(location.latitude, location.longitude))
                .rotation(bearing)
                .anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location))

        myMarker = mMap?.addMarker(markerOptions1)

    }

    override fun onCameraIdle() {

        if (flag == 1) {

            sourceLat = mMap?.cameraPosition?.target?.latitude
            sourceLong = mMap?.cameraPosition?.target?.longitude

            textView_source.text = sourceLat?.let {
                sourceLong?.let { it1 -> getCompleteAddressString(context, it, it1) }

            }
        } else {
            destinationLat = mMap?.cameraPosition?.target?.latitude
            destinationLong = mMap?.cameraPosition?.target?.longitude

            textView_destination.text = destinationLat?.let {
                destinationLong?.let { it1 -> getCompleteAddressString(context, it, it1) }

            }
        }


    }


    private fun checkPermissions(): Boolean {
        return if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermissions()
            false
        }
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1)
    }

    companion object {
        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }

    fun getCompleteAddressString(context: Context, LATITUDE: Double, LONGITUDE: Double): String {
        var strAdd = ""
        val geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                Log.e(TAG, "My Current: " + addresses.toString())
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder("")
                if (returnedAddress.maxAddressLineIndex > 0) {

                    for (i in 0 until returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ")
                    }
                } else {
                    strReturnedAddress.append(returnedAddress.getAddressLine(0)).append("")
                }
                strAdd = strReturnedAddress.toString()
                Log.e(TAG, "My Current" + strReturnedAddress.toString())
            } else {
                Log.e(TAG, "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(TAG, "Canont get Address!")
        }

        return strAdd
    }

    private fun fetchUrl() {
        doAsync {
            val s = URL(getProxyDirection(sourceLat, sourceLong, destinationLat, destinationLong)).readText()
            uiThread {
                val jsonObj = JSONObject(s)
                if (!jsonObj.optString("status").equals("ZERO_RESULTS", ignoreCase = true)) {
                    parserTask(listOf(s))
                }
            }
        }
    }

    private fun parserTask(s: List<String>) {
        doAsync {
            val jObject: JSONObject
            var routes: List<HashMap<String, String>>? = null

            try {
                jObject = JSONObject(s[0])
                Log.d("ParserTask", s[0])
                parser = DataParser()
                Log.d("ParserTask", parser.toString())

                // Starts parsing data
                routes = parser.parse(jObject)
                Log.d("ParserTask", "Executing routes")
                Log.d("ParserTask", routes.toString())

            } catch (e: java.lang.Exception) {
                Log.d("ParserTask", e.toString())
                e.printStackTrace()
            }


            uiThread {
                try {

                    var points: ArrayList<LatLng>? = null
                    var lineOptions: PolylineOptions? = null

                    if (routes != null) {
                        // Traversing through all the routes
                        if (routes.isNotEmpty()) {
                            for (i in routes.indices) {
                                points = ArrayList()
                                lineOptions = PolylineOptions()

                                // Fetching i-th route
                                var path: List<HashMap<String, String>>? = null
                                path = listOf(routes[i])

                                // Fetching all the points in i-th route
                                for (j in path.indices) {
                                    val point = path.get(j)

                                    val lat = java.lang.Double.parseDouble(point.get("lat"))
                                    val lng = java.lang.Double.parseDouble(point.get("lng"))
                                    val position = LatLng(lat, lng)

                                    points.add(position)
                                }

                                // Adding all the points in the route to LineOptions
                                lineOptions.addAll(points)
                                lineOptions.width(5f)
                                lineOptions.color(Color.BLACK)

                                Log.d("onPostExecute", "onPostExecute lineoptions decoded")

                            }
                            if (!sourceLat?.equals("")!! && !sourceLat?.equals("")!!) {
                                val location = sourceLong?.let { it1 -> LatLng(sourceLat!!, it1) }
                                //mapReset();
                                if (sourceMarker != null)
                                    sourceMarker!!.remove()

                                val markerOptions = location?.let { it1 ->
                                    MarkerOptions()
                                            .position(it1)
                                            .title("source").draggable(true)
                                            .icon(bitmapDescriptorFromVector(context, R.drawable.ic_map_source_marker)).draggable(false)
                                }
                                marker = mMap?.addMarker(markerOptions)
                                sourceMarker = mMap?.addMarker(markerOptions)
                                //CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(18).build();
                                //mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }
                            if (!destinationLat?.equals("")!! && !destinationLong?.equals("")!!) {
                                val destLatLng = LatLng(destinationLat!!, destinationLong!!)
                                if (destinationMarker != null)
                                    destinationMarker!!.remove()
                                // Setup camera movement
                                /*final int width = getResources().getDisplayMetrics().widthPixels;
                            final int height = getResources().getDisplayMetrics().heightPixels;
                            final int minMetric = Math.min(width, height);*/
                                val destMarker = MarkerOptions()
                                        .position(destLatLng).title("destination")
                                        .draggable(true)
                                        .icon(bitmapDescriptorFromVector(context, R.drawable.ic_map_source_marker)).draggable(false)
                                destinationMarker = mMap!!.addMarker(destMarker)
                                val builder = LatLngBounds.Builder()
                                builder.include(sourceMarker!!.getPosition())
                                builder.include(destinationMarker!!.getPosition())
                                val bounds = builder.build()

                            }
                        }


                    }
                } catch (e: java.lang.Exception) {

                }
            }
        }


    }

    fun getProxyDirection(source_latitude: Double?, source_longitude: Double?, dest_latitude: Double?, dest_longitude: Double?): String {

        // Origin of route
        val str_origin = "origin=$source_latitude,$source_longitude"

        // Destination of route
        val str_dest = "destination=$dest_latitude,$dest_longitude"

        val sensor = "sensor=false"
        val output = "json"

        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor"

        // Building the url to the web service
        Log.d("directionUrl", "https://maps.googleapis.com/maps/api/directions/$output?$parameters&key=${applicationContext.resources.getString(R.string.api_key)}")
        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters&key=${applicationContext.resources.getString(R.string.api_key)} "
    }

    fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

}
