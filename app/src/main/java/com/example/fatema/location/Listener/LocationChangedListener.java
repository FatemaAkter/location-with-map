package com.example.fatema.location.Listener;

import android.location.Location;

public interface LocationChangedListener {
    void onLocationChanged(Location location);
}
