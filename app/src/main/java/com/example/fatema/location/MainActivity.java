package com.example.fatema.location;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView textView_multiple_typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView_multiple_typeface = findViewById(R.id.textView_multiple_typeface);

        String firstWord = "৳ ";
        String secondWord = "second";

// Create a new spannable with the two strings
        Spannable spannable = new SpannableString(firstWord+secondWord);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/bariol_regular-webfont.ttf");
        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/SolaimanLipi.ttf");

// Set the custom typeface to span over a section of the spannable object
        spannable.setSpan( new CustomTypefaceSpan("sans-serif",font2), 0, firstWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan( new CustomTypefaceSpan("sans-serif",font), firstWord.length(), firstWord.length() + secondWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the text of a textView with the spannable object
        textView_multiple_typeface.setText( spannable );
    }
}
