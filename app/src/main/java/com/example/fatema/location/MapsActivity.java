package com.example.fatema.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fatema.location.Kotlin.DataParser;
import com.example.fatema.location.Listener.LocationChangedListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.ButtCap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationChangedListener, GoogleMap.OnCameraIdleListener {

    private GoogleMap mMap;
    private Marker myMarker;
    float bearing = 0f;
    Location prevLocation;
    int value = 0;
    private String TAG = "MapsActivity";
    Double sourceLat;
    Double sourceLong;
    Double destinationLat;
    Double destinationLong;
    Context context= MapsActivity.this;
    int flag = 1;
    DataParser parser;
    Marker sourceMarker;
    Marker marker;
    Marker destinationMarker;

    @BindView(R.id.textView_destination)
    TextView textView_destination;
    @BindView(R.id.textView_source)
    TextView textView_source;
    @BindView(R.id.imageView_marker)
    ImageView imageView_marker;
    ParserTask parserTask;
    FetchUrl fetchUrl;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        initMap();
        BaseAppController.getInstance().initLocationListener(this);
        BaseAppController.getInstance().initLocation();
        BaseAppController.getInstance().getLastLocation();
        BaseAppController.getInstance().startLocationUpdates();
        imageView_marker.setImageResource(R.drawable.ic_map_source_marker);

    }

    @OnClick(R.id.textView_source)
    public void textViewSourceClick() {
        imageView_marker.setImageResource(R.drawable.ic_map_source_marker);
        flag = 1;
    }

    @OnClick(R.id.textView_destination)
    public void textViewDestinationClick() {
        imageView_marker.setImageResource(R.drawable.ic_map_destination_marker);
        flag = 2;
    }

    @OnClick(R.id.button_get_direction)
    public void getDirectionClick() {
        fetchUrl = new FetchUrl("");
        if (destinationLat != null && destinationLong != null) {
            fetchUrl.execute(getGoogleDirectionURL(sourceLat, sourceLong, destinationLat, destinationLong));
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d("Location", "" + location);
        if (value == 0) {
            LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(16f).build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            /*    //frmSourceLoad.setVisibility(View.VISIBLE);
              Utilities.getAddressUsingLatLng("source", frmSource, context, "" + latitude, "" + longitude, addressListener, true);*/
            value++;

        }
        if (prevLocation == null) {
            prevLocation = location;

        }
        bearing = prevLocation.bearingTo(location);
        if (myMarker != null) {
            myMarker.remove();
        }

        MarkerOptions markerOptions1 = new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .rotation(bearing)
                .anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location));

        myMarker = mMap.addMarker(markerOptions1);

    }

    @Override
    public void onCameraIdle() {
        if (flag == 1) {

            sourceLat = mMap.getCameraPosition().target.latitude;
            sourceLong = mMap.getCameraPosition().target.longitude;

            textView_source.setText(getCompleteAddressString(sourceLat, sourceLong));
        } else {
            destinationLat = mMap.getCameraPosition().target.latitude;
            destinationLong = mMap.getCameraPosition().target.longitude;


            textView_destination.setText(getCompleteAddressString(destinationLat, destinationLong));

        }


    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkPermissions()) {
            LatLng latLng = new LatLng(23.810332, 100.4125181);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(16f).build();

            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.setOnCameraIdleListener(this);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.setMinZoomPreference(11f);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.setBuildingsEnabled(true);
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setRotateGesturesEnabled(false);
            mMap.getUiSettings().setTiltGesturesEnabled(false);

        }
        mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.style_json));

    }

    public void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions();
            return false;
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction", "Canont get Address!");
        }
        return strAdd;
    }

    public static String getGoogleDirectionURL(double source_latitude, double source_longitude, double dest_latitude, double dest_longitude) {


        // Origin of route
        String str_origin = "origin=" + source_latitude + "," + source_longitude;

        // Destination of route
        String str_dest = "destination=" + dest_latitude + "," + dest_longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = UrlHelper.GET_DIRECTION + output + "?" + parameters + "&key=" + UrlHelper.GOOGLE_MAP_API_KEY;


        return url;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {
        String approx;

        public FetchUrl(String approx) {
            this.approx = approx;
        }

        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";
            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObj = new JSONObject(result);
                if (!jsonObj.optString("status").equalsIgnoreCase("ZERO_RESULTS")) {
                    parserTask = new ParserTask(approx);
                    // Invokes the thread for parsing the JSON data
                    parserTask.execute(result);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        com.example.fatema.location.DataParser parser;
        String approx;

        public ParserTask(String approx) {
            this.approx = approx;
        }

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                parser = new com.example.fatema.location.DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            try {

                ArrayList<LatLng> points = null;
                PolylineOptions lineOptions = null;

                if (result != null) {
                    // Traversing through all the routes
                    if (result.size() > 0) {
                        for (int i = 0; i < result.size(); i++) {
                            points = new ArrayList<>();
                            lineOptions = new PolylineOptions();

                            // Fetching i-th route
                            List<HashMap<String, String>> path = result.get(i);

                            // Fetching all the points in i-th route
                            for (int j = 0; j < path.size(); j++) {
                                HashMap<String, String> point = path.get(j);

                                double lat = Double.parseDouble(point.get("lat"));
                                double lng = Double.parseDouble(point.get("lng"));
                                LatLng position = new LatLng(lat, lng);

                                points.add(position);
                            }

                            // Adding all the points in the route to LineOptions
                            lineOptions.addAll(points);
                            lineOptions.width(5);
                            lineOptions.color(Color.BLACK);

                            Log.d("onPostExecute", "onPostExecute lineoptions decoded");

                        }


                        if (sourceLat != null && sourceLong != null) {
                            LatLng location = new LatLng(sourceLat, sourceLong);
                            //mapReset();
                            if (sourceMarker != null)
                                sourceMarker.remove();

                            MarkerOptions markerOptions = new MarkerOptions()
                                    .position(location)
                                    .title("source").draggable(true)
                                    .icon(bitmapDescriptorFromVector(R.drawable.ic_map_source_marker)).draggable(false);
                            marker = mMap.addMarker(markerOptions);
                            sourceMarker = mMap.addMarker(markerOptions);
                            //CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(18).build();
                            //mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                        if (destinationLat != null && destinationLong != null) {
                            LatLng destLatLng = new LatLng(destinationLat, destinationLong);
                            if (destinationMarker != null)
                                destinationMarker.remove();
                            // Setup camera movement
                            /*final int width = getResources().getDisplayMetrics().widthPixels;
                            final int height = getResources().getDisplayMetrics().heightPixels;
                            final int minMetric = Math.min(width, height);*/
                            MarkerOptions destMarker = new MarkerOptions()
                                    .position(destLatLng).title("destination")
                                    .icon(bitmapDescriptorFromVector(R.drawable.ic_map_destination_marker)).draggable(false);
                            destinationMarker = mMap.addMarker(destMarker);
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            builder.include(sourceMarker.getPosition());
                            builder.include(destinationMarker.getPosition());
                            LatLngBounds bounds = builder.build();

                        }

                    }
                    if (lineOptions != null && points != null) {
                        //mMap.addPolyline(lineOptions);
                        startAnim(points);
                    } else {
                        Log.d("onPostExecute", "without Polylines drawn");
                    }

                }



             /*   // Drawing polyline in the Google Map for the i-th route
                if (flowValue != 0) {
                    if (lineOptions != null && points != null) {
                        //mMap.addPolyline(lineOptions);
                        startAnim(points);
                    } else {
                        Log.d("onPostExecute", "without Polylines drawn");
                    }
                }*/
            } catch (Exception e) {


            }
        }

    }

    private void startAnim(ArrayList<LatLng> routeList) {
        if (mMap != null && routeList.size() > 1) {
            MapAnimator.getInstance().animateRoute(context, mMap, routeList);
        }
    }

    public BitmapDescriptor bitmapDescriptorFromVector( int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BaseAppController.getInstance().stopLocationUpdates();
    }
}
